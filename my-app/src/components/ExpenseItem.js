import React, { useState } from "react";
import "./ExpenseItem.css";
import ExpenseDate from "./ExpenseDate";

function ExpenseItem(prop) {
  const [title, setTitle] = useState(prop.title);

  const changeTitle = () => {
    return setTitle("updated");
  };
  return (
    <div className="expense-item">
      <ExpenseDate date={prop.date} />
      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">{prop.amount}</div>
      </div>
      <button onClick={changeTitle}>Change</button>
    </div>
  );
}
export default ExpenseItem;
