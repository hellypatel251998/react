import React, { useState } from "react";
import NewExpense from "./NewExpense";
import "./NewExpenseForm.css";
const NewExpenseForm = (prop) => {
  const getData = (enteredData) => {
    const data = { ...enteredData, id: Math.random() };
    editing(false);
    return data;
  };

  const [isEditing, editing] = useState(false);

  const getForm = () => {
    editing(true);
  };
  const stopEditing = () => {
    return editing(false);
  };
  return (
    <div className="new-expense">
      {!isEditing && <button onClick={getForm}>Add Item</button>}
      {isEditing && <NewExpense onSave={getData} onCancel={stopEditing} />}
    </div>
  );
};

export default NewExpenseForm;
