import React, { useState } from "react";
import ExpenseItem from "./ExpenseItem";
import ExpensesFilter from "./ExpensesFilter";

function DisplayItem(prop) {
  const [filteredState, setFilteredState] = useState("2020");
  const getChoosenYear = (choosenYear) => {
    const year = choosenYear;
    setFilteredState(year);
    return year;
  };

  const filterdExpenses = prop.expenses.filter(
    (expense) => expense.date.getFullYear().toString() === filteredState
  );
  return (
    <div>
      <h2>Let's get started!</h2>
      <ExpensesFilter default={filteredState} onChoosenYear={getChoosenYear} />
      {filterdExpenses.length === 0 && <p>"no item to show"</p>}

      {filterdExpenses.length > 0 &&
        filterdExpenses.map((expense) => (
          <ExpenseItem
            title={expense.title}
            date={expense.date}
            amount={expense.amount}
          />
        ))}
    </div>
  );
}
export default DisplayItem;
