import React, { useState } from "react";
import "./NewExpense.css";

const NewExpense = (prop) => {
  const [getTitle, setTitle] = useState("");
  const [getDate, setDate] = useState("");
  const [getAmount, setamount] = useState("");

  const SaveTitle = (event) => {
    return setTitle(event.target.value);
  };
  const SaveDate = (event) => {
    return setDate(event.target.value);
  };
  const SaveAmount = (event) => {
    return setamount(event.target.value);
  };

  const SubmitForm = (event) => {
    event.preventDefault();
    const expenseData = {
      title: getTitle,
      amount: getAmount,
      date: new Date(getDate),
    };

    prop.onSave(expenseData);
    setTitle("");
    setDate("");
    setamount("");
  };

  return (
    <form onSubmit={SubmitForm}>
      <div className="new-expense__control">
        <label>Tital:</label>
        <input type="text" value={getTitle} onChange={SaveTitle} />
      </div>

      <div className="new-expense__control">
        <label>Date:</label>
        <input
          type="date"
          min="2020-01-01"
          max="2022-01-01"
          value={getDate}
          onChange={SaveDate}
        />
      </div>
      <div className="new-expense__control">
        <label>Amount:</label>
        <input type="number" value={getAmount} onChange={SaveAmount}></input>
      </div>
      <div className="new-expense__control">
        <button type="submit">Submit</button>
      </div>
      <div className="new-expense__control">
        <button onClick={prop.onCancel}>Cancel</button>
      </div>
    </form>
  );
};
export default NewExpense;
